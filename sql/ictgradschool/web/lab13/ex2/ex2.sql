# Answers to exercise 2 questions
-- A
SELECT s.fname,s.lname
    FROM unidb_students AS s, unidb_attend AS a
    WHERE s.id=a.id AND a.dept = 'COMP' AND a.num = 219
;
-- B
SELECT s.fname,s.lname
    FROM unidb_students AS s, unidb_courses AS c
    WHERE country <> 'NZ' AND s.id=c.rep_id
;
-- C
SELECT l.office
    FROM unidb_lecturers AS l, unidb_teach AS t
    WHERE t.staff_no=l.staff_no AND t.num=219
;
-- D
SELECT DISTINCT s.fname,s.lname
    FROM unidb_students AS s, unidb_attend AS a, unidb_teach AS t, unidb_lecturers AS l
    WHERE s.id=a.id AND l.staff_no=t.staff_no AND a.dept=t.dept AND l.fname = 'Te Taka'
;
-- E
SELECT s2.fname AS studentfname, s2.lname AS studentlname, s.fname AS mentorfname, s.lname AS mentorlname
    FROM unidb_students AS s, unidb_students AS s2
    WHERE s.id=s2.mentor
;
-- F
SELECT l.fname,l.lname
    FROM unidb_lecturers AS l
    WHERE l.office like 'G.%'
UNION
SELECT s.fname,s.lname
    FROM unidb_students AS s
    WHERE country <> 'NZ'
;
-- G
SELECT l.fname AS coordinatorfname, l.lname AS coordinatorlname, s.fname AS repfname, s.lname AS replname
    FROM unidb_courses AS c, unidb_lecturers AS l,unidb_students AS s
    WHERE c.coord_no=l.staff_no AND c.rep_id=s.id AND c.dept='COMP' AND c.num=219