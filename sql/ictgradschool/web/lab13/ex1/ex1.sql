# Answers to exercise 1 questions
-- A
SELECT DISTINCT c.dept
FROM sample_db.unidb_courses AS c
;
-- B
SELECT DISTINCT a.semester
FROM sample_db.unidb_attend AS a
;
-- C
SELECT DISTINCT c.descrip
FROM sample_db.unidb_attend AS a, sample_db.unidb_courses AS c
WHERE a.dept = c.dept AND a.num=c.num
;
-- D
SELECT s.fname,s.lname,s.country
FROM sample_db.unidb_students AS s
ORDER BY s.fname ASC
;
-- E
SELECT s.fname,s.lname, s.mentor
FROM sample_db.unidb_students AS s
ORDER BY s.mentor ASC
;
-- F
SELECT *
FROM sample_db.unidb_lecturers AS l
ORDER BY l.office ASC
;
-- G
SELECT *
FROM sample_db.unidb_lecturers AS l WHERE l.staff_no>500
;
-- H
SELECT *
FROM sample_db.unidb_students AS s
WHERE s.id BETWEEN 1668 AND 1824
;
-- I
SELECT *
FROM sample_db.unidb_students AS s
WHERE s.country IN ('NZ', 'AU', 'US')
;
-- J
SELECT *
FROM sample_db.unidb_lecturers AS l  WHERE l.office like 'G.%'
;
-- K
SELECT *
    FROM sample_db.unidb_courses AS c
    WHERE c.dept <> 'COMP'
;
-- L
SELECT *
    FROM sample_db.unidb_students AS s
    WHERE country IN ('FR','MX')