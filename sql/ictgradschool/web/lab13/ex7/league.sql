SET FOREIGN_KEY_CHECKS =0;
-- to ignore the foreign key check before deleting the tables

/* ALTER TABLE lab13_players DROP FOREIGN KEY lab13_players_ibfk_1;
ALTER TABLE lab13_teams DROP FOREIGN KEY lab13_teams_ibfk_1; */
-- dropping the foreign key (another alternative)


DROP TABLE IF EXISTS lab13_players;
DROP TABLE IF EXISTS lab13_teams;

CREATE TABLE lab13_teams (
    id        INT(3)      NOT NULL,
    name      VARCHAR(20) NOT NULL,
    captainid INT(3) NOT NULL,
    points    INT(3) DEFAULT 0,
    homecity  VARCHAR(32) NOT NULL,
    PRIMARY KEY (id),
    UNIQUE (name)
);

CREATE TABLE lab13_players (
    playerid    INT(3)      NOT NULL,
    fname       VARCHAR(32) NOT NULL,
    lname       VARCHAR(32) NOT NULL,
    age         INT(2)      NOT NULL CHECK (age >= 16),
    nationality VARCHAR(32) NOT NULL,
    teamid      INT(3)      NOT NULL,
    PRIMARY KEY (playerid),
    FOREIGN KEY (teamid) REFERENCES lab13_teams (id)
);


INSERT INTO lab13_teams (id, name, captainid, points , homecity) VALUES
    (1, 'ManUnited',12,23, 'Manchester'),
    (002, 'ManCity',23,31, 'Manchester');


INSERT INTO lab13_players (playerid,fname,lname,age,nationality,teamid) VALUES
    (012, 'David', 'Beckham', 30, 'England', 001),
    (001, 'Apple', 'Juice', 30, 'England', 001),
    (002, 'Orange', 'Pie', 30, 'China', 001),
    (003, 'Olivia', 'Tea', 25, 'India', 001),
    (4, 'Allen','Random',22,'Sri Lanka',001),
    (023, 'John', 'John', 20, 'New Zealand', 002);

ALTER TABLE lab13_teams ADD FOREIGN KEY (captainid) REFERENCES lab13_players(playerid);

SET FOREIGN_KEY_CHECKS =1;
-- set it back to check the foreign key constraint
